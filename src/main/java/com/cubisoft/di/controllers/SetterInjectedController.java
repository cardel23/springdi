package com.cubisoft.di.controllers;

import com.cubisoft.di.services.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

/**
 * Created by pwk04 on 11-21-20
 */
@Controller
public class SetterInjectedController {

    @Autowired
    @Qualifier("setterGreetingService") // Este indica que servicio implementara la interfaz
    private GreetingService greetingService;

    public void setGreetingService(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String getGreeting(){
        return  greetingService.sayGreeting();
    }
}
