package com.cubisoft.di.controllers;

import com.cubisoft.di.services.GreetingService;
import org.springframework.stereotype.Controller;

/**
 * Created by pwk04 on 11-21-20
 */
@Controller
public class DepInjController {

    private final GreetingService greetingService;

    public DepInjController(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String sayHello(){
        return greetingService.sayGreeting();
    }
}
