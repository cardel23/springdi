package com.cubisoft.di.services;

/**
 * Created by pwk04 on 11-21-20
 */

public interface GreetingService {

    String sayGreeting();
}
