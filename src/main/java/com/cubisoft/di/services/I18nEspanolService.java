package com.cubisoft.di.services;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Created by Carlos Delgadillo on 21/11/20
 */
@Service("i18nService")
@Profile("ES")
public class I18nEspanolService implements GreetingService {
    @Override
    public String sayGreeting() {
        return "ola k ase";
    }
}

