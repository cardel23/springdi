package com.cubisoft.di.services;

import org.springframework.stereotype.Service;

/**
 * Created by pwk04 on 11-21-20
 */
@Service
public class PropertyGreetingService implements GreetingService {
    @Override
    public String sayGreeting() {
        return "ola k ase - Property";
    }
}
