package com.cubisoft.di.services;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Created by pwk04 on 11-21-20
 */
@Service("i18nService")
@Profile({"EN","default"})
public class I18nEnglishService implements GreetingService {
    @Override
    public String sayGreeting() {
        return "henlo wat you doin";
    }
}
