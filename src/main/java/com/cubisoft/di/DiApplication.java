package com.cubisoft.di;

import com.cubisoft.di.controllers.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class DiApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(DiApplication.class, args);
		/**
		 * Busca lo que esta anotado como beans y lo puede agregar al contexto poniendo el nombre de la clase iniciando
		 * en minusculas
		 *
		 * Para evitar errores no usar clases que inicien con mas de 2 mayusculas
		 */
		I18nController i18nController = (I18nController)context.getBean("i18nController");
		System.out.println(i18nController.getGreeting());
		DepInjController controller = (DepInjController) context.getBean("depInjController");
		String saludo = controller.sayHello();
		System.out.println("---------- Primary");
		System.out.println(saludo);
		System.out.println("---------- Property");
		PropertyInjectedController propertyInjectedController = (PropertyInjectedController) context.getBean("propertyInjectedController");
		System.out.println(propertyInjectedController.getGreeting());
		System.out.println("---------- Setter");
		SetterInjectedController setterInjectedController = (SetterInjectedController) context.getBean("setterInjectedController");
		System.out.println(setterInjectedController.getGreeting());
		System.out.println("---------- Constructor");
		ConstructorInjectedController constructorInjectedController = (ConstructorInjectedController) context.getBean("constructorInjectedController");
		System.out.println(constructorInjectedController.getGreeting());


	}

}
